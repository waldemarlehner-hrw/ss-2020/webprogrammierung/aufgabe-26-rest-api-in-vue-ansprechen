module.exports = {
	publicPath: process.env.NODE_ENV === 'production' ? '/ss-2020/webprogrammierung/aufgabe-26-rest-api-in-vue-ansprechen/' : '/',
	configureWebpack: {
		module: {
			rules: [
				{
					test: /\.scss$/,
					use: [
						'vue-style-loader',
						'css-loader',
						'sass-loader'
					]
				}
			]
		}
	}
}